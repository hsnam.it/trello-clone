const express = require('express');
const app = new express();

require('dotenv').config();

app.get('/', (req, res) => {
    res.send('You know, for Trello API!');
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});